<?php
	require_once 'google-api/src/Google/Client.php';
	require_once 'google-api/src/Google/Service/YouTube.php';

	class YouTubeFinder {

		private $key = NULL;

		public function setCredentials($key) {
			$this->key = $key;
		}

		public function getLinks($keywords, $hd = TRUE, $min_views = 2000, $min_comments = 10, $max_rank = 50, 
													$thumbnail = "high") {
			$videoResults = array();
			$links = array();

			$client = new Google_Client();
			$client->setApplicationName("YouTubeFinder");
			$client->setDeveloperKey($this->key);
	
			$service = new Google_Service_YouTube($client); 

			$optParams = array('maxResults' => $max_rank, 'type' => 'video', 'videoDefinition' => 'high', 'q' => $keywords);
			$results = $service->search->listSearch('snippet', $optParams);

			foreach( $results as $item ) {
				array_push($videoResults, $item['id']['videoId']);
			}

			$videoIds = join(',', $videoResults);

			unset($optParams);
			unset($results);

			$optParams = array('id' => $videoIds);
			$results = $service->videos->listVideos('snippet,statistics', $optParams);	

			foreach( $results as $item ) {
				if ( $item['statistics']['commentCount'] > $min_comments && $item['statistics']['viewCount'] > $min_views ) {
					array_push($links, array( 'title' => $item['snippet']['title'], 
									'desc' => $item['snippet']['description'],
									'date' => $item['snippet']['publishedAt'],
									'url' => "//www.youtube.com/embed/" . $item['id'],
									'author' => $item['snippet']['channelTitle'],
									'views' => $item['statistics']['viewCount'],
									'thumbnail' => $item['snippet']['thumbnails'][$thumbnail]['url']
								));
				}
			}  

			return $links;
		}
	}
?>
